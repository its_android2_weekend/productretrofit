package com.example.productretrofit.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.productretrofit.PreferenceUtil;
import com.example.productretrofit.R;
import com.example.productretrofit.ServiceGenerator;
import com.example.productretrofit.api.ProductServices;
import com.example.productretrofit.entityResponse.Data;
import com.example.productretrofit.entityResponse.DataItem;
import com.example.productretrofit.entityResponse.ProductDetailResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AppCompatActivity {
    private TextView tvProductName;
    private TextView tvQty;
    private TextView tvPrice;

    private int productId;
    private ProductServices services;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        initView();

        Intent intent = getIntent();
        productId = intent.getIntExtra("productId",0);
        services = ServiceGenerator.createService(ProductServices.class, PreferenceUtil.ReadTokenPreference(this));

        //Back Button in Action Bar;
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        initDetail(productId);
    }

    private void initView() {
        tvProductName = findViewById(R.id.tvProductName);
        tvQty = findViewById(R.id.tvQty);
        tvPrice = findViewById(R.id.tvPrice);
    }

    private void initDetail(int productId) {
        services.findProductDetail(productId).enqueue(new Callback<ProductDetailResponse>() {
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                if(response.isSuccessful()){
                    Data item = response.body().getData();
                    setTitle(item.getProductName());//change activity Titlte;
                    tvProductName.setText("Product Name: "+ item.getProductName());
                    tvQty.setText("Quantity: "+item.getQty());
                    tvPrice.setText("Price: "+ item.getPrice());
                }else{
                    Toast.makeText(ProductDetailActivity.this, "Error mapping", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(ProductDetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

}
