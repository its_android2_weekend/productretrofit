package com.example.productretrofit.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.productretrofit.PreferenceUtil;
import com.example.productretrofit.R;
import com.example.productretrofit.ServiceGenerator;
import com.example.productretrofit.adapter.ProductAdapter;
import com.example.productretrofit.api.ProductRecyclerViewListener;
import com.example.productretrofit.api.ProductServices;
import com.example.productretrofit.entityResponse.DataItem;
import com.example.productretrofit.entityResponse.ProductResponse;
import com.example.productretrofit.entityResponse.ProductListResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements ProductRecyclerViewListener {
    private static final String TAG = "000000";
    private ProductServices services;
    private ProductAdapter productAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<DataItem> dataItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        productAdapter = new ProductAdapter();
        dataItems = new ArrayList<>();

        int userStatus = PreferenceUtil.ReadUserStatusPreference(this);
        Intent i = null;
        switch (userStatus){
            case 1:
                i = new Intent(this,LoginRegisterActivity.class);
                startActivity(i);
                finish();
                break;
        }
        services = ServiceGenerator.createService(ProductServices.class,PreferenceUtil.ReadTokenPreference(this));
        productAdapter.setListener(this); //Assign reference to RecyclerView adapter;
        initView();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e(TAG,"onRefresh");
                initView();
            }
        });
    }

     void initView() {
        Log.e(TAG, "onResponse");
        Call<ProductListResponse> call =  services.findAllProducts();
        call.enqueue(new Callback<ProductListResponse>() {
            @Override
            public void onResponse(Call<ProductListResponse> call, Response<ProductListResponse> response) {
                Log.e(TAG, "onResponse");
                if(response.isSuccessful()){
                    Log.e(TAG, "isSuccessful");
                    Log.e(TAG,response.body().getData().toString());
                    //Setup Recyclerview;
                    productAdapter.clear();
                    dataItems = response.body().getData();
                    productAdapter.addMoreItem(dataItems);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    recyclerView.setAdapter(productAdapter);
                    swipeRefreshLayout.setRefreshing(false);
                }
                else
                {
                    Log.e(TAG, "Error");
                }
            }
            @Override
            public void onFailure(Call<ProductListResponse> call, Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.LogOut:
                PreferenceUtil.SetUserStatusPreference(1,this);
                PreferenceUtil.SetTokenPreference(null,this);
                startActivity(new Intent(this,LoginRegisterActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProductItemClick(int position) {
        Toast.makeText(this, "Item "+ dataItems.get(position).toString(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,ProductDetailActivity.class);
        intent.putExtra("productId",dataItems.get(position).getId());
        startActivity(intent);
    }

    @Override
    public void onProductItemDeleteClick(int position) {
        Toast.makeText(this, "Delete was click at item index "+ position, Toast.LENGTH_SHORT).show();
        final DataItem item = dataItems.get(position);//get current item that we selected;
        new AlertDialog.Builder(this)
                .setTitle("Delete Confirm")
                .setMessage("Do you really want to delete this product?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(MainActivity.this, "Yes is Click", Toast.LENGTH_SHORT).show();
                        //Delete Action;
                        services.deleteProduct(item.getId()).enqueue(new Callback<ProductResponse>() {
                            @Override
                            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                                if(response.isSuccessful()){
                                    ProductResponse deleteResponse = response.body();
                                    if(!deleteResponse.getMessage().equals("Unauthenticated.")){
                                        Toast.makeText(MainActivity.this, deleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                        initView();
                                    }
                                    else{
                                        Toast.makeText(MainActivity.this, deleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ProductResponse> call, Throwable t) {
                                t.printStackTrace();
                                Toast.makeText(MainActivity.this, "Error found", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }
}
