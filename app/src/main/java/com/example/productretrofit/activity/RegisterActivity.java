package com.example.productretrofit.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.productretrofit.PreferenceUtil;
import com.example.productretrofit.R;
import com.example.productretrofit.ServiceGenerator;
import com.example.productretrofit.api.UserServices;
import com.example.productretrofit.entityResponse.UserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "000000";
    private EditText etName;
    private EditText etEmail;
    private EditText etPassword;

    private UserServices service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        service = ServiceGenerator.createService(UserServices.class,null);

        etEmail = findViewById(R.id.etEmail);
        etName = findViewById(R.id.etName);
        etPassword = findViewById(R.id.etPassword);


        findViewById(R.id.btnRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etName.getText().toString();
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                RegisterAccount(name,email,password);
            }
        });
    }

    private void RegisterAccount(String name, String email, String password) {
        Call<UserResponse> call =  service.signUp(
                name,
                email,
                password);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    Log.e(TAG,"TOKEN= "+ response.body().getToken());
                    Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else
                    Toast.makeText(RegisterActivity.this, "Signin Failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });
    }
}
