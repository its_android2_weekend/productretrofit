package com.example.productretrofit.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.productretrofit.PreferenceUtil;
import com.example.productretrofit.R;
import com.example.productretrofit.ServiceGenerator;
import com.example.productretrofit.api.UserServices;
import com.example.productretrofit.entityResponse.UserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "00000";
    private Button btnLogin;
    private EditText etEmail;
    private EditText etPassword;
    private UserServices service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //bind view reference;
        btnLogin = findViewById(R.id.btnSignIn);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin.setOnClickListener(this);

        service = ServiceGenerator.createService(UserServices.class,null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSignIn:
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                CheckLogin(email,password);
                break;
        }
    }

    private void CheckLogin(String email, String password) {
        Call<UserResponse> call =  service.signIn(
                email,
                password);

        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    //Step 1: Save token inSharePreference;
                    //Step 2: Close Login => Open MainActivity
                    Log.e(TAG,"TOKEN= "+ response.body().getToken());
                    PreferenceUtil.SetTokenPreference(response.body().getToken(),LoginActivity.this);
                    Toast.makeText(LoginActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    PreferenceUtil.SetUserStatusPreference(2,LoginActivity.this);
                }else{
                    Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.e(TAG,"onFailure");
                t.printStackTrace();
            }
        });
    }
}
