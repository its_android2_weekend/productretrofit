package com.example.productretrofit.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.productretrofit.PreferenceUtil;
import com.example.productretrofit.R;
import com.example.productretrofit.ServiceGenerator;
import com.example.productretrofit.api.ProductServices;
import com.example.productretrofit.entityResponse.ProductResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddProductActivity extends AppCompatActivity {
    private Button btnAddproduct;
    private ProductServices services;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        btnAddproduct = findViewById(R.id.btnAdd);

        services = ServiceGenerator.createService(ProductServices.class, PreferenceUtil.ReadTokenPreference(this));

        btnAddproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                services.addProduct("iPhone 11 Pro Max",100,2000).enqueue(new Callback<ProductResponse>() {
                    @Override
                    public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                        ProductResponse deleteResponse = response.body();
                        if(!deleteResponse.getMessage().equals("Unauthenticated.")){
                            Toast.makeText(AddProductActivity.this, deleteResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(AddProductActivity.this, deleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        });

    }
}
