package com.example.productretrofit.entityResponse;

import com.google.gson.annotations.SerializedName;

public class ProductResponse {

	@SerializedName("message")
	private String message;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"ProductResponse{" +
			"message = '" + message + '\'' + 
			"}";
		}
}