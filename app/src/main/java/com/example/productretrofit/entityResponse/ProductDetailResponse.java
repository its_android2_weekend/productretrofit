package com.example.productretrofit.entityResponse;

import com.google.gson.annotations.SerializedName;

public class ProductDetailResponse{

	@SerializedName("data")
	private Data data;

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ProductDetailResponse{" + 
			"data = '" + data + '\'' + 
			"}";
		}
}