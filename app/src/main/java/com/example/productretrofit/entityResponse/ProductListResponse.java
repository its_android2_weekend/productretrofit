package com.example.productretrofit.entityResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductListResponse {

	@SerializedName("data")
	private List<DataItem> data;

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ProductListResponse{" +
			"data = '" + data + '\'' + 
			"}";
		}
}