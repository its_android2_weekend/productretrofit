package com.example.productretrofit.api;

public interface ProductRecyclerViewListener {
    void onProductItemClick(int position);
    void onProductItemDeleteClick(int position);
}
