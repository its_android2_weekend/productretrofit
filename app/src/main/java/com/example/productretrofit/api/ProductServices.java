package com.example.productretrofit.api;

import com.example.productretrofit.entityResponse.ProductResponse;
import com.example.productretrofit.entityResponse.ProductDetailResponse;
import com.example.productretrofit.entityResponse.ProductListResponse;
import com.example.productretrofit.entityResponse.UserResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ProductServices {
    @GET("api/products")
    Call<ProductListResponse> findAllProducts();

    @GET("api/products/{id}")
    Call<ProductDetailResponse> findProductDetail(@Path("id") int productId);

    @DELETE("/api/products/{id}")
    Call<ProductResponse> deleteProduct(@Path("id") int productId);

    @FormUrlEncoded
    @POST("api/products")
    Call<ProductResponse> addProduct(
            @Field("product_name") String ProductName,
            @Field("qty") int qty,
            @Field("price") double price
    );
}
