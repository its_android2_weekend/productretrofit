package com.example.productretrofit;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUtil {
    private static final String token_key = "token";
    private static final String user_status = "userStatus";

    private static final String preference_key = "userdata";

    public static void SetTokenPreference(String strToken, Context context){
        if(context !=null){
            SharedPreferences preferences = context.getSharedPreferences(preference_key,Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(token_key,strToken);
            editor.commit();
        }
    }

    public static String ReadTokenPreference(Context context){
        SharedPreferences preferences = context.getSharedPreferences(preference_key,Context.MODE_PRIVATE);
        return preferences.getString(token_key,null);
    }

    public static void SetUserStatusPreference(int status, Context context){
        if(context !=null){
            SharedPreferences preferences = context.getSharedPreferences(preference_key,Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(user_status,status);
            editor.commit();
        }
    }

    public static int ReadUserStatusPreference(Context context){
        SharedPreferences preferences = context.getSharedPreferences(preference_key,Context.MODE_PRIVATE);
        return preferences.getInt(user_status,1);
    }
}
