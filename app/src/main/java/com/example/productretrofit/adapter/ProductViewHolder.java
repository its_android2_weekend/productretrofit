package com.example.productretrofit.adapter;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.productretrofit.R;
import com.example.productretrofit.api.ProductRecyclerViewListener;
import com.example.productretrofit.entityResponse.DataItem;

public class ProductViewHolder extends RecyclerView.ViewHolder {
    private TextView tvDisplayName;
    private ImageView ivDelete;
    private ProductRecyclerViewListener listener;

    public ProductViewHolder(@NonNull View itemView, final ProductRecyclerViewListener listener) {
        super(itemView);
        tvDisplayName = itemView.findViewById(R.id.tvDisplayName);
        ivDelete = itemView.findViewById(R.id.ivDelete);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("0000000","event Click");
                if(listener ==  null) return;
                listener.onProductItemClick(getAdapterPosition());
            }
        });
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener ==  null) return;
                listener.onProductItemDeleteClick(getAdapterPosition());
            }
        });
    }

    public void onBind(DataItem dataItem) {
        tvDisplayName.setText(dataItem.getProductName());
    }
}
