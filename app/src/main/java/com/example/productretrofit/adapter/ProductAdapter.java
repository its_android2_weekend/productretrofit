package com.example.productretrofit.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.productretrofit.R;
import com.example.productretrofit.api.ProductRecyclerViewListener;
import com.example.productretrofit.entityResponse.DataItem;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductViewHolder>  {
    List<DataItem> dataItems;
    private ProductRecyclerViewListener listener;

    public ProductAdapter() {
        dataItems = new ArrayList<>();
    }


    public void setListener(ProductRecyclerViewListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false);
        return new ProductViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        holder.onBind(dataItems.get(position));
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }
    public void addMoreItem(List<DataItem> dataItems){
        this.dataItems = dataItems;
        notifyDataSetChanged();
    }
    public void clear(){
        this.dataItems.clear();
        notifyDataSetChanged();
    }

}
